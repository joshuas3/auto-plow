# espy

A wheelchair motor-propelled, battery-powered, ESP32-driven remote control snow plow.

![Parked snow plow](https://joshstock.in/static/images/rc-plow.jpg)
